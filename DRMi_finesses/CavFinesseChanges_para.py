import matplotlib
matplotlib.use('qt5agg')

from ipyparallel import Client
import datetime as dt

import DRMiFinesse as df

from pykat import finesse
from pykat.commands import *
import pykat.external.peakdetect as peak
import pykat.ifo.aligo as aligo
import pykat.ifo.aligo.plot as aligoplt
import gwinc as gwinc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
# pykat.init_pykat_plotting(dpi=90)


def main():
    print("""
    -------------------------------
       CavFinesseChanges_para.py
       
        Anna Green 2020.03.31
    -------------------------------   
     
    """)
    #plot all the sensitivity curves to check the results as we go?
    nannymode=True
    #range of scale factors to apply to ITM transmissions>
    sfs = np.linspace(1,0.5,5)
    #build models for each sf with HOMs up to HG orders: (-1=plane waves)
    maxtems = [-1]
    
    #base reference model
    aLIGO = aligo.make_kat()
    TT=0.325 #better match to sites for SRM reflectivity
    aLIGO.SRM.setRTL(1-TT-aLIGO.SRM.L,TT,aLIGO.SRM.L)
    
    
    if nannymode:
        #check the original object was ok to start with.
        print("check: plotting strain sensitivity...")
        aLIGO.maxtem = -1 #USE PLANE WAVES MODEL FIRST (speed, complexity...)
        aLIGO=aligo.setup(aLIGO)
        fig,ax=plt.subplots()
        freqs=np.logspace(1,4,1000)
        aligoplt.strain_sensitivity(aLIGO,ax=ax,
                                    lower=freqs[0],upper=freqs[-1],steps=len(freqs),
                                    plot_cmds={'label':'design.kat (no sqz, m0, SRM_T=32.5%)'})
        plt.legend(loc=2,bbox_to_anchor=(1,1))
        plt.show()#block=False
        #set up a helpful colourway we can reference later.
        cols=cm.jet(np.linspace(0,1,len(sfs)))
    
    for mm in maxtems:
        model = aLIGO.deepcopy()
        model.maxtem=mm
        model=aligo.setup(model)
        print('\nmaxtem: {}'.format(model.maxtem))
        print("reference (default) values: ")
        df.printTs(model)
        df.getPF(model,printit=True)
        refvals=df.getPF(model)
        refPow=refvals[1]
        reffp = df.DARM_TF(model,optics=["LX","LY"],getfp ='fp2')
        print()
        
        if nannymode:
            fig , ax  = plt.subplots(len(sfs),1,tight_layout=True,sharex=True,sharey=True,figsize=(10,len(sfs)*3)) #QNLS
                                 
        for sf,i in zip(sfs,range(len(sfs))):
            
            base, base1, base2, filename = restoreQNLS(sf,mm,model,refPow,reffp)
            print("final kat file saved to {}".format(filename))
            
            if nannymode:
                #with no alteration:
                aligoplt.strain_sensitivity(model,ax=ax[i],plot_cmds={'label':'design.kat (no sqz)','ls':'--','lw':2})
                #after changing ITMs:
                aligoplt.strain_sensitivity(base,ax=ax[i],plot_cmds={'label':'ITM transmissions changed','lw':1.5,'ls':':'})
                #after changing PRM to match arm power:
                aligoplt.strain_sensitivity(base1,ax=ax[i],plot_cmds={'label':'restoring Parm'})
                #after changing SRM to match pole:
                aligoplt.strain_sensitivity(base2,ax=ax[i],plot_cmds={'label':'restoring DARM pole'})
                #plot stylings
                ax[i].set_title("sf={:.2f}: ITMX_T={:.3g}=ITMY_T".format(sf,float(base.ITMX.T)),color=cols[i])
                ax[i].legend(loc=2,bbox_to_anchor=(1,1))
        
        if nannymode:
            fig.show()
    
        print("-------------------------------------------------")
        
    input("I mean I'm done when you are tbh...")
    
    return


    
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
def restoreQNLS(sf,mm,model,refPow,reffp):
    filename = "temp/m{}sf{:.3f}.kat".format(mm,sf)
    
    print('-------------')
    base=model.deepcopy()
    ###change ITM reflectivities
    base.ITMX.setRTL(1-model.ITMX.T*sf-model.ITMX.L,model.ITMX.T*sf,model.ITMX.L)
    base.ITMY.setRTL(1-model.ITMY.T*sf-model.ITMY.L,model.ITMY.T*sf,model.ITMY.L)
    print("ITMX_T = {:.3g} (=ITMY_T). running setup()".format(float(base.ITMX.T)))
    base = aligo.setup(base) #VERY NECESSARY TO MAINTAIN RESONANCE!
    # cavvals=df.getPF(base)#<---------is this necessary?
    ##tune PRM transmission to get back to original arm power
    print('restoring Parm to target of {:.0f}W with PRM_T...'.format(refPow),end=' ')
#         T,P = tuneT(base,optic='PRM',node='nETMX1',val=0.03,precision=0.025,
#               target_precision=None,target=refPow,debug=False) #<---------Looping OFF for now - VV slow here
    T,P = df.quicktuneT2(base,optic='PRM',node='nETMX1',val=0.05,precision=0.045,target_precision=None,target=refPow,debug=False)#<looping off, not setup() in this version but difference is minor
    base1=base.deepcopy()
    base1.PRM.setRTL(1-T-base1.PRM.L, T,base1.PRM.L)
    print('PRM_T = {:.4f}. running setup()'.format(T))
    base1 = aligo.setup(base1)
    # cavvals1=df.getPF(base1) #<-------------is this necessary?
    ##tune SRM transmission to get the sensitivity curve back
    print('restoring DARM pole to target of {:.3f}Hz using SRM_T...'.format(reffp),end='  ')
    T_SRM, fp = df.tuneTfp_I2(base1,target=reffp,loss = model.SRM.L)#,debug=True
    base2=base1.deepcopy()
    base2.SRM.setRTL(1-T_SRM-base2.PRM.L, T_SRM ,base2.PRM.L)
    print('SRM_T = {:.4f}. running setup()'.format(T_SRM))
    base2 = aligo.setup(base2)
    # cavvals2=df.getPF(base2)#<------------is this necessary?
    base2.save(filename)
    return base,base1,base2,filename
    



def myfunc(T):
    from pykat import finesse
    k=finesse.kat()
    k.verbose=False
    k.parse("""
    l L0 1 0 n0
    gauss mybeam L0 n0 100u 0 101u 1m
    maxtem 4
    s s0 0 n0 n1
    m m 0.5 0.5 0 n1 nout
    pd P nout
    xaxis L0 P lin 1 100 10000""")
    k.m.setRTL(1-T,T,0)
    o=k.run()
    return o

def RunInSeries(function,variants, timeit=True):
    if timeit:
        T0 = dt.datetime.now()
        
    outs=[]
    for v in variants:
        outs.append(function(v))
        
    if timeit:
        print("duration: {}".format(dt.datetime.now()-T0))

    return outs


def RunInParallel(function,variants,mode='balanced',timeit=True):
    if timeit:
        T0 = dt.datetime.now()
        
    rc=Client() #class object to start the client to the parallel cluster
    if mode=='balanced':
        lview = rc.load_balanced_view()#creates a DirectView object with load-balanced execution using all engines
    else:
        lview = rc.direct_view()#creates a DirectView object
    lview.block = False # if self.block is False, returns AsyncResult, else: returns actual result of f(*args, **kwargs) on the engine(s)
    results = [lview.apply_async(function,yy) for yy in variants]
    outs = [d.get() for d in results]
        
    if timeit:
        print("duration: {}".format(dt.datetime.now()-T0))

    return outs
    
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
if __name__ == '__main__':
    main()