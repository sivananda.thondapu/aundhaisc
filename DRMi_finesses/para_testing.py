from ipyparallel import Client
import datetime as dt

def main():
    print("""
    -------------------------------
            para_testing.py
            
    test script showing speed 
    differences when running kat 
    objects in parallel, but in a 
    more general ipyparallel way
    than parakat.
    
    NB: running from terminal is 
    recommended for parallel jobs 
    to prevent memory clogging.
    (ipyparallel bug on some 
    systems)
       
        Anna Green 2020.03.31
    -------------------------------   
     
    """)
    vals = range(1,10)
    testvals = [v/10 for v in vals]
    
    print("series")
    o=RunInSeries(myfunc,testvals)
    input('cont?')#force wait until I record the memory used: 192.0MB
    o=None #clear memory if we can?: 79.3MB - so yes pm. (71.8MB when first pressed enter on this cell)
    input('cont?')
    
    
    print("parallel")
    o=RunInParallel(myfunc,testvals)
    input('cont?')#force wait until I record the memory used: 155.1MB
    o=None #clear memory if we can: nope.
    input('cont?')
    
    print("worse parallel")
    o=RunInParallel(myfunc,testvals,mode='DirectView')
    input('cont?')#force wait until I record the memory used: 917.3MB + 500MB or so in *one* of the engines...
    o=None #clear memory if we can: nope, neither.
    #directview seems worse in every respect. huh. Fan was trying to take off like crazy too.
    #think this just completely crashed my jupyter kernal too?? Or maybe just too much heavy lifting..?
    return


    
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
def myfunc(T):
    from pykat import finesse
    k=finesse.kat()
    k.verbose=False
    k.parse("""
    l L0 1 0 n0
    gauss mybeam L0 n0 100u 0 101u 1m
    maxtem 4
    s s0 0 n0 n1
    m m 0.5 0.5 0 n1 nout
    pd P nout
    xaxis L0 P lin 1 100 100000""")
    k.m.setRTL(1-T,T,0)
    o=k.run()
    return o

def RunInSeries(function,variants, timeit=True):
    if timeit:
        T0 = dt.datetime.now()
        
    outs=[]
    for v in variants:
        outs.append(function(v))
        
    if timeit:
        print("duration: {}".format(dt.datetime.now()-T0))

    return outs


def RunInParallel(function,variants,mode='balanced',timeit=True):
    if timeit:
        T0 = dt.datetime.now()
        
    rc=Client() #class object to start the client to the parallel cluster
    if mode=='balanced':
        lview = rc.load_balanced_view()#creates a DirectView object with load-balanced execution using all engines
    else:
        lview = rc.direct_view()#creates a DirectView object
    lview.block = False # if self.block is False, returns AsyncResult, else: returns actual result of f(*args, **kwargs) on the engine(s)
    results = [lview.apply_async(function,yy) for yy in variants]
    outs = [d.get() for d in results]
        
    if timeit:
        print("duration: {}".format(dt.datetime.now()-T0))

    return outs
    
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
if __name__ == '__main__':
    main()